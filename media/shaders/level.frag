#version 150

in vec2 TexCoord;

out vec4 outColor;

uniform sampler2D tex;

void main() {

  outColor = texture(tex, TexCoord);
  if(outColor.r == 1.f && outColor.b == 1.f && outColor.g == 0.f) {
    outColor = vec4(1.f, 1.f, 1.f, 0.f);
  }

}
