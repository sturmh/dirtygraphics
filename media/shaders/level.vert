#version 150

in vec2 pos;
in vec2 uv;

out vec2 TexCoord;

uniform mat4 trans;
uniform mat4 viewProj;

void main() {
    TexCoord = uv;
    gl_Position = viewProj * trans * vec4(pos, 0.0, 1.0);
}