//
//  GLFWInput.h
//  fcukyou
//
//  Created by Eugene Sturm on 2/23/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __fcukyou__GLFWInput__
#define __fcukyou__GLFWInput__

#include "GLFWWindow.h"
#include "Keycode.h"
#include <vector>


namespace application {
    typedef void (*KeyCallback)(int, int);
    typedef void (*MousePosCallback)(double, double);
    typedef void (*MouseButtonCallback)(int, int);

    class GLFWInput {
        friend class GLFWWindow;
    public:
        static void RegisterKeyCallback(KeyCallback keyCallback);
        static void RegisterMousePosCallback(MousePosCallback mousePosCallback);
        static void RegisterMouseButtonCallback(MouseButtonCallback mouseButtonCallback);
    protected:
        static void ProcessKey(int key, int action);
        static void ProcessMouseButton(int btn, int action);
        static void ProcessMousePosition(int x, int y);
    private:
        static KeyCallback _keyCallback;
        static MousePosCallback _mousePosCallback;
        static MouseButtonCallback _mouseButtonCallback;
    };
}

#endif /* defined(__fcukyou__GLFWInput__) */
