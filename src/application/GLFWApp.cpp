//
//  GLFWApp.cpp
//  dirtyGraphics
//
//  Created by Eugene Sturm on 2/22/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "GLFWApp.h"
#include <cstdio>
using namespace application;

GLFWApp* GLFWApp::_app = 0;

GLFWApp::GLFWApp() {
    _window = 0;
    glfwInit();
    
}

GLFWApp::GLFWApp(GLFWApp const&) {
    //do nothing
}

GLFWApp& GLFWApp::operator=(GLFWApp const&) {
    //do nothing
}

GLFWApp::~GLFWApp() {
#warning cleanup timers
    glfwTerminate();
}

GLFWWindow* GLFWApp::createWindow(int width, int height) {
    if(_window) {
        printf("(GLFWApp::createWindow) Window has already been created. Returning existing window\n");
        return _window;
    }
    
    _window = new GLFWWindow(width, height);
    return _window;
}

GLFWWindow* GLFWApp::getWindow() {
    if(_window) {
        return _window;
    } else {
        printf("(GLFWApp::getWindow() Attempting to retrieve window that does not exists\n");
        return 0;
    }
}

GLFWTimer* GLFWApp::createTimer(std::string identifier) {
    if(_timerMap[identifier]) {
        printf("(GLFWApp::createTimer) Timer with identifier %s already exists, not creating new timer\n", identifier.c_str());
        return _timerMap[identifier];
    } else {
        GLFWTimer* timer = new GLFWTimer();
        _timerMap[identifier] = timer;
        return timer;
    }
}

GLFWTimer* GLFWApp::getTimer(std::string identifier) {
    if(_timerMap[identifier]) {
        return _timerMap[identifier];
    } else {
        printf("(GLFWApp::getTimer) Timer with identifier %s does not exist\n", identifier.c_str());
        return 0;
    }
}

void GLFWApp::destroyTimer(std::string identifier) {
    if(_timerMap[identifier]) {
        GLFWTimer* timer = _timerMap[identifier];
        _timerMap.erase(identifier);
        delete timer;
    } else {
        printf("(GLFWApp::destroyTimer) Timer with identifier %s does not exist\n", identifier.c_str()); 
    }
}

void GLFWApp::destroyTimer(GLFWTimer* timer) {
    bool destroyed = false;
    for(TimerMap::iterator it = _timerMap.begin(); it != _timerMap.end(); ++it) {
        if(it->second == timer) {
            GLFWTimer* timer = it->second;
            _timerMap.erase(it->first);
            delete timer;
            destroyed = true;
        }
    }
    
    if(destroyed == false) {
        printf("(GLFWApp::destroyTimer) Failed to destroy timer\n");
    }
}

GLFWApp* GLFWApp::getGLFWApp() {
    if(_app == 0) {
        _app = new GLFWApp();
    }
    return _app;
}
