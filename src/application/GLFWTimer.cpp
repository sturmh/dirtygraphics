//
//  GLFWTimer.cpp
//  fcukyou
//
//  Created by Eugene Sturm on 2/23/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "GLFWTimer.h"
using namespace application;

GLFWTimer::GLFWTimer() {
    _startTime = 0;
}

double GLFWTimer::getTime() {
    return glfwGetTime() - _startTime;
}

void GLFWTimer::reset() {
    _startTime = glfwGetTime();
}