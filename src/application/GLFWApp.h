//
//  GLFWApp.h
//  dirtyGraphics
//
//  Created by Eugene Sturm on 2/22/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtyGraphics__GLFWApp__
#define __dirtyGraphics__GLFWApp__


#include "GLFWWindow.h"
#include "GLFWTimer.h"
#include <iostream>
#include <map>

namespace application {
    
    typedef std::map<std::string, GLFWTimer*> TimerMap;
    
    class GLFWApp {
    public:        
        GLFWWindow* createWindow(int width, int height);
        GLFWWindow* getWindow();
        
        GLFWTimer* createTimer(std::string identifier);
        GLFWTimer* getTimer(std::string identifier);
        void destroyTimer(std::string identifier);
        void destroyTimer(GLFWTimer* timer);
        
        static GLFWApp* getGLFWApp();
    private:
        GLFWApp();
        GLFWApp(GLFWApp const&);             // copy constructor is private
        ~GLFWApp();
        GLFWApp& operator=(GLFWApp const&);   // assignment operator is private
    private:
        static GLFWApp* _app;
        GLFWWindow* _window;
        TimerMap _timerMap;
        
    };
};

#endif /* defined(__dirtyGraphics__GLFWApp__) */
