//
//  GLFWInput.cpp
//  fcukyou
//
//  Created by Eugene Sturm on 2/23/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "GLFWInput.h"
using namespace application;

KeyCallback GLFWInput::_keyCallback = 0;
MousePosCallback GLFWInput::_mousePosCallback = 0;
MouseButtonCallback GLFWInput::_mouseButtonCallback = 0;

void GLFWInput::ProcessKey(int key, int action) {
    if(_keyCallback) {
        _keyCallback(key, action);
    }
}

void GLFWInput::ProcessMouseButton(int btn, int action) {
    if(_mouseButtonCallback) {
        _mouseButtonCallback(btn, action);
    }
}

void GLFWInput::ProcessMousePosition(int x, int y) {
#warning map x/y to our own coords
    if(_mousePosCallback) {
        _mousePosCallback(x, y);
    }
}

void GLFWInput::RegisterKeyCallback(KeyCallback keyCallback) {
    _keyCallback = keyCallback;
}

void GLFWInput::RegisterMousePosCallback(MousePosCallback mousePosCallback) {
    _mousePosCallback = mousePosCallback;
}

void GLFWInput::RegisterMouseButtonCallback(MouseButtonCallback mouseButtonCallback) {
    _mouseButtonCallback = mouseButtonCallback;
}