//
//  GLFWWindow.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/12/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__GLFWWindow__
#define __dirtygraphics__GLFWWindow__

#ifdef __APPLE__
  #include <gl/glfw.h>
#else
  #include <GL/glfw.h>
#endif

#include "GLFWInput.h"
#include "Config.h"

namespace application {
    
    enum VSync {
        VSYNC_DISABLE = 0,
        VSYNC_ENABLE = 1
    };

    class GLFWWindow {
    public:
        GLFWWindow(int width, int height);
        ~GLFWWindow();
    
        void GetDimensions(int* width, int* height);
        int IsActive();
        int IsOpen();
        void EnableVsync();
        void DisableVsync();
        void SwapBuffers();
        void HideMouseCursor();
        void ShowMouseCursor();
    private:
        int _width;
        int _height;
    };
}

#endif /* defined(__dirtygraphics__GLFWWindow__) */
