//
//  GLFWWindow.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/12/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "GLFWWindow.h"
using namespace application;

GLFWWindow::GLFWWindow(int width, int height) {
	
    _width = width;
    _height = height;
    
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, GL_VERSION_MAJOR);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, GL_VERSION_MINOR);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GL_PROFILE);
    
    glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
    glfwOpenWindow(_width, _height, 0, 0, 0, 0, 24, 8, GLFW_WINDOW);
    
    glfwSetWindowTitle("OpenGL");
    glfwSwapInterval(VSYNC_ENABLE);
    
    glfwSetKeyCallback(GLFWInput::ProcessKey);
    glfwSetMousePosCallback(GLFWInput::ProcessMousePosition);
    glfwSetMouseButtonCallback(GLFWInput::ProcessMouseButton);
    
}



void GLFWWindow::GetDimensions(int* width, int* height) {
    *width = _width;
    *height = _height;
}

int GLFWWindow::IsActive() {
    return glfwGetWindowParam(GLFW_ACTIVE);
}

int GLFWWindow::IsOpen() {
    return glfwGetWindowParam(GLFW_OPENED);
}

void GLFWWindow::HideMouseCursor() {
    glfwDisable( GLFW_MOUSE_CURSOR );
}

void GLFWWindow::ShowMouseCursor() {
    glfwEnable( GLFW_MOUSE_CURSOR );
}

GLFWWindow::~GLFWWindow() {

}

void GLFWWindow::SwapBuffers() {
    glfwSwapBuffers();
}

void GLFWWindow::EnableVsync() {
    glfwSwapInterval(VSYNC_ENABLE);
}

void GLFWWindow::DisableVsync() {
    glfwSwapInterval(VSYNC_DISABLE);
}
