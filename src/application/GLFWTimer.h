//
//  GLFWTimer.h
//  fcukyou
//
//  Created by Eugene Sturm on 2/23/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __fcukyou__GLFWTimer__
#define __fcukyou__GLFWTimer__

#ifdef __APPLE__
  #include <gl/glfw.h>
#else
  #include <GL/glfw.h>
#endif

namespace application {
    class GLFWTimer {
    public:
        GLFWTimer();
        
        /**
         * Restarts the timer
         **/
        void reset();
        
        /**
         * @return time in seconds since creation of window or last reset call
         */
        double getTime();
    private:
        double _startTime;
    };
}

#endif /* defined(__fcukyou__GLFWTimer__) */
