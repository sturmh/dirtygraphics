//
//  main.cpp
//  DirtyGraphics
//
//  Created by Eugene Sturm on 2/8/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//
#include "../graphics/DGRenderer.h"
#include <gl/glfw.h>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include <string>
#include "../ext/log.h"
#include "../application/GLFWApp.h"
#include "../application/GLFWInput.h"
#include "../graphics/DGMap.h"
#include "../animation/DAAnimationManager.h"
#include <list>
#include <vector>
using namespace application;
using namespace dirtyAnimation;
using namespace dirtygraphics;

int keyStates[512] = {0};
void ProcessInput(int key, int action) {
    keyStates[key] = action;
    printf("%d, %d\n", key, action);
}

void ProcessMouseButton(int btn, int action) {
    printf("%d, %d\n", btn, action);
}

void ProcessMousePos(double x, double y) {
    printf("x:%d, y:%d\n", x, y);
}

int main(int argc, const char * argv[])
{
    GLFWApp* app = GLFWApp::getGLFWApp();
	std::string dir = "";
	int half;
    
	#if defined(__APPLE__)
		dir = "/Users/sturm/Projects/dirtygraphics";
		half = 1;
	#elif defined(_WIN32)
		dir = "C:/Users/marks_000/git/dirtygraphics";
		half = 2;
	#else
		std::cout << "system not found" << std::endl;
		return 1;
	#endif
    
    //create window
    GLFWWindow* window = app->createWindow(800, 600);
    GLFWInput::RegisterKeyCallback(ProcessInput);
    GLFWInput::RegisterMouseButtonCallback(ProcessMouseButton);
    GLFWInput::RegisterMousePosCallback(ProcessMousePos);
    window->EnableVsync();
    
    GLFWTimer* timer = app->createTimer("timer1");



    //create renderer and get managers
    dirtygraphics::Renderer renderer(window, dir);
    
    std::string graphics = "4980 Graphics Main Demo";
    std::string graphicst = "graphicsTag";

    dirtygraphics::SceneManager* sm = renderer.GetSceneManager();
    dirtygraphics::ResourceManager* rm = renderer.GetResourceManager();


    //load some resources
    rm->LoadTexture("walking", "/media/images/walkingSpriteSheet.png");
    rm->LoadTexture("tiles", "/media/images/tilesetdraft.png");
    
    srand(time(NULL));
    //create map
    Tileset* tileset = sm->CreateTileset("tileset1");
    tileset->imageName = "tiles";
    tileset->renderedTileHeight = 32;
    tileset->renderedTileWidth = 32;
    tileset->tileHeight = 16;
    tileset->tileWidth = 16;
    tileset->spaceX = 0;
    tileset->spaceY = 0;
    tileset->maxCol = 10;
    tileset->maxRow = 20;

    Map* map = sm->CreateMap("b");
    map->tileset = tileset;
    map->rows = 100;
    map->cols = 100;
    map->priorityLevel = 2;
    
    //load tiles
    for(int i = 0; i < map->rows; ++i) {
        std::vector<DrawableTile> tss;

        map->tiles.push_back(tss);
        for(int j = 0; j < map->cols; ++j) {
            DrawableTile t;
            t.col = rand() % 10;
            t.row = rand() % 20;
            t.visible = rand() % 20 < 10 ? 0 : 1;
            map->tiles[i].push_back(t);
        }
    }

    //submit map -> creates geometry and will now be rendererd
    sm->SubmitMap("b");
    
    
    
    map = sm->CreateMap("a");
    map->tileset = tileset;
    map->rows = 100;
    map->cols = 100;
    map->priorityLevel = 1;
    
    //load tiles
    for(int i = 0; i < map->rows; ++i) {
        std::vector<DrawableTile> tss;
        
        map->tiles.push_back(tss);
        for(int j = 0; j < map->cols; ++j) {
            DrawableTile t;
            t.col = rand() % 2;
            t.row = rand() % 2;
            t.visible = 1;//rand() % 20 < 10 ? 0 : 1;
            map->tiles[i].push_back(t);
        }
    }
    
    //submit map -> creates geometry and will now be rendererd
    sm->SubmitMap("a");

    
    //create sprite
    dirtygraphics::Sprite* s2 = sm->CreateSprite("sprite");
    s2->Dimensions(40, 40);
    s2->SetTexture("walking");
    s2->SetSize(32,32);

    //create camera and set as active
    dirtygraphics::Camera* cam = sm->CreateCamera("cam1");
    sm->SetActiveCamera("cam1");
    cam->Move(0, 0);

    //setup animation
    AnimationManager* animationManager = new AnimationManager();
    SpriteAnimation* aniW = animationManager->CreateSpriteAnimation("moveUp",true);
    aniW->AddState(glm::vec2(0,3),.25);
    aniW->AddState(glm::vec2(1,3),.25);
    aniW->AddState(glm::vec2(2,3),.25);
    aniW->AddState(glm::vec2(1,3),.25);
    
    SpriteAnimation* aniA = animationManager->CreateSpriteAnimation("moveLeft",true);
    aniA->AddState(glm::vec2(0,1),.25);
    aniA->AddState(glm::vec2(1,1),.25);
    aniA->AddState(glm::vec2(2,1),.25);
    aniA->AddState(glm::vec2(1,1),.25);
    
    SpriteAnimation* aniS = animationManager->CreateSpriteAnimation("moveDown",true);
    aniS->AddState(glm::vec2(0,0),.25);
    aniS->AddState(glm::vec2(1,0),.25);
    aniS->AddState(glm::vec2(2,0),.25);
    aniS->AddState(glm::vec2(1,0),.25);
    

    SpriteAnimation* aniD = animationManager->CreateSpriteAnimation("moveRight",true);
    aniD->AddState(glm::vec2(0,2),.25);
    aniD->AddState(glm::vec2(1,2),.25);
    aniD->AddState(glm::vec2(2,2),.25);
    aniD->AddState(glm::vec2(1,2),.25);
    

    float speed = 200.0f;
    float camSpeed = 200.0f;
    float partialFrame = 0;

    //Setup Text;

    
    //STRINGS MUST BE INITIALIZED BEFORE RENDERER
    dirtygraphics::Text text = dirtygraphics::Text();
    	text.SetColor(0.0,0.0,1.0);
    	text.SetText(graphics);
    	text.SetPosition(-0.75,0.5);
        renderer.Submit(graphicst, &text);

    s2->AttachCamera(cam);
    
    int frames = 0;
	while(window->IsOpen())
    {
        if (keyStates[257]) {
            break;
        }
        
        float frameTime = timer->getTime();
        //std::cout << "FPS:" << 1/frameTime <<std::endl;
        char buffer[20];
        sprintf(buffer, "%f", 1.f/frameTime);
        text.SetText(buffer);
        partialFrame = (partialFrame+frameTime);
        if (partialFrame > 1) partialFrame -= 1;
        timer->reset();
            
        //Gather Input
        if (keyStates[283])
            cam->Move(0.f,camSpeed*frameTime);
        if (keyStates[284])
            cam->Move(0.f,-camSpeed*frameTime);
        if (keyStates[285])
            cam->Move(-camSpeed*frameTime,0.f);
        if (keyStates[286])
            cam->Move(camSpeed*frameTime,0.f);
        
        //Update sprite
        if (keyStates[87]) { //w
            s2->Move(0.f,speed*frameTime);
            s2->SetAnimation(aniW);
        }
        else if (keyStates[83]) { //a
            s2->Move(0.f,-speed*frameTime);
            s2->SetAnimation(aniS);
        }
        else if (keyStates[65]) { //s
            s2->Move(-speed*frameTime,0.f);
            s2->SetAnimation(aniA);
        }
        else if (keyStates[68]) { //d
            s2->Move(speed*frameTime,0.f);
            s2->SetAnimation(aniD);
        }
        else {
            s2->SetAnimation(0);
        }
        
        s2->Animate(frameTime);
        
        if(keyStates[90]) {
            cam->Zoom(1.25f);
        }
        if(keyStates[88]) {
            cam->Zoom(0.75f);
        }
        
        if(keyStates[84]) {
            s2->AttachCamera(cam);
        }
        if(keyStates[89]) {
        	s2->DetachCamera();
        }

        //draw stuffs
        renderer.Draw();
        
    }
    
    glfwTerminate();

    return 0;
}


