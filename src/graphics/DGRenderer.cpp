//
//  Renderer.cpp
//  DirtyGraphics
//
//  Created by Eugene Sturm on 2/8/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGRenderer.h"
#include <iostream>
using namespace dirtygraphics;
using namespace application;

Renderer::Renderer(application::GLFWWindow* window, std::string workingDirectory) {
    _window = window;
    _levelOverlay = false;
    _passabilityOverlay = false;
    
    FILELog::ReportingLevel() = logDEBUG3;
	std::string logdir = workingDirectory + "/logs/graphicslog.txt";
	FILE* log_fd = fopen( logdir.c_str(), "w" );
	Output2FILE::Stream() = log_fd;
	std::cout << "Renderer Loading" << std::endl;

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if(!err == GL_FALSE) {
        printf("GLEW Initialization failed. Exiting.");
        std::cout << glewGetString(err) << std::endl;
        FILE_LOG(logERROR) << "GLEW initialization failed. Exiting.";
        return;
    }
    
    FILE_LOG(logINFO) << "Vendor: " << glGetString(GL_VENDOR);
    FILE_LOG(logINFO) << "Renderer: " << glGetString(GL_RENDERER);
    FILE_LOG(logINFO) << "Renderer: " << glGetString(GL_VERSION);
    FILE_LOG(logINFO) << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION);
    
    printf("Vendor:   %s\n", glGetString(GL_VENDOR));
    printf("Renderer: %s\n", glGetString(GL_RENDERER));
    printf("Version:  %s\n", glGetString(GL_VERSION));
    printf("GLSL:     %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    _sceneManager = new SceneManager(this);
    _resourceManager = new ResourceManager(workingDirectory);
    
    //glViewport(0, 0, 100, 100);
    
    glClearColor(0.5f, 0.5f, 0.5f, 1.f);
    
    FILE_LOG(logINFO) << "(Renderer::Renderer) Blend Mode: Enabled";
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //set up freetype
	if(FT_Init_FreeType(&_ft)) {
		FILE_LOG(logERROR) << "(Renderer::Renderer) FreeType Init Failed";
	}

	//set up font
	//TODO Move this to resource manager
	std::string fontdir = workingDirectory + "/assets/fonts/Eadui.ttf";
	if(FT_New_Face(_ft, fontdir.c_str(), 0, &_face)) {
		FILE_LOG(logERROR) << "(Renderer::Renderer) FreeType Font Load Failed";
	}

	//set up font size
	//TODO Move this somewhere better
	if (FT_Set_Pixel_Sizes(_face, 0, 72)) {
		FILE_LOG(logERROR) << "(Renderer::Renderer) FreeType Font Size Failure";
	}


}

Renderer::~Renderer() {
    delete _resourceManager;
    delete _sceneManager;
}

SceneManager* Renderer::GetSceneManager() {
    return _sceneManager;
}

ResourceManager* Renderer::GetResourceManager() {
    return _resourceManager;
}

void Renderer::SetCamera(Camera* cam) {
    _camera = cam;
}

void Renderer::Submit(std::string name, Sprite* sprite) {
    _spriteMap[name] = sprite;
}

void Renderer::Submit(std::string name, Map* map) {
    GeometryFactory g;
    _resourceManager->CreateGeometry(name, g.GenerateMap(map, &_resourceManager->GetTexture(map->tileset->imageName)->info));
    _maps[name] = map;
}

void Renderer::Submit(std::string name, Map* map, core::PassabilityMap* pmap) {
	GeometryFactory g;
    _resourceManager->CreateGeometry("pass", g.GeneratePasabilityMap(map, &_resourceManager->GetTexture(map->tileset->imageName)->info, pmap));
}

void Renderer::Submit(std::string name, Text* text) {
	_textMap[name] = text;
}

void Renderer::RemoveSprite(std::string name) {
    _spriteMap.erase(name);
}

void Renderer::RemoveMap(std::string name) {
    _maps.erase(name);
}

void Renderer::RemoveText(std::string name) {
	_textMap.erase(name);
}

void Renderer::DrawGUI() {
    std::pair<GLuint, ElementCount> g = _resourceManager->GetVAO("square");
    GLuint program = _resourceManager->GetProgram("gui");
    
    glUseProgram(program);
    glBindVertexArray(g.first);
    
    glm::vec2 scale = glm::vec2();
    scale.x = 0.25f;
    scale.y = 1.f;
    glUniform2fv(glGetUniformLocation(program, "scale"), 1, glm::value_ptr(scale));
    glDrawElements(GL_TRIANGLES, g.second, GL_UNSIGNED_INT, 0);
    
    glUseProgram(0);
    glBindVertexArray(0);
}

void Renderer::DrawText() {
	//load text shaders
	GLuint program = _resourceManager->GetProgram("text");
	glUseProgram(program);

	//set pixel alignment to draw text cleanly.
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Create Vertex Array Object
	GLuint vao;
	glGenVertexArrays( 1, &vao );
	glBindVertexArray( vao );

	// Create an element array
	GLuint ebo;
	glGenBuffers( 1, &ebo );

	GLuint elements[] = {
				0, 1, 2,
				2, 3, 0
			};

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ebo );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( elements ), elements, GL_STATIC_DRAW );

	// Create a Vertex Buffer Object and copy the vertex data to it
	GLuint vbo;
	glGenBuffers( 1, &vbo );

	glBindBuffer( GL_ARRAY_BUFFER, vbo );

	// Specify the layout of the vertex data
	GLint posAttrib = glGetAttribLocation( program, "position" );
	glEnableVertexAttribArray( posAttrib );
	glVertexAttribPointer( posAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof( float ), 0 );

	GLint colAttrib = glGetAttribLocation( program, "color" );
	glEnableVertexAttribArray( colAttrib );
	glVertexAttribPointer( colAttrib, 3, GL_FLOAT, GL_FALSE, 7 * sizeof( float ), (void*)( 2 * sizeof( float ) ) );

	GLint texAttrib = glGetAttribLocation( program, "texcoord" );
	glEnableVertexAttribArray( texAttrib );
	glVertexAttribPointer( texAttrib, 2, GL_FLOAT, GL_FALSE, 7 * sizeof( float ), (void*)( 5 * sizeof( float ) ) );

	glActiveTexture(GL_TEXTURE5);
	// Load texture
	GLuint tex;
	glGenTextures( 1, &tex );

	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	glUniform1i( glGetUniformLocation( program, "tex" ), 5 );

	FT_GlyphSlot glyph = _face->glyph;
	int width, height;
	_window->GetDimensions(&width, &height);
	float sx = 2.f/(float)width;
	float sy = 2.f/(float)height;

	for(TextMap::iterator it = _textMap.begin(); it != _textMap.end(); ++it) {
	    Text* text = it->second;

	    //initialize position and color
	    float x = text->GetPosition().x;
	    float y = text->GetPosition().y;
	    float r = text->GetColor().r;
	    float g = text->GetColor().g;
	    float b = text->GetColor().b;

	    const char *p;
			for (p = text->GetText().c_str(); *p; p++) {
				if(FT_Load_Char(_face, *p, FT_LOAD_RENDER))
						continue;

				float x2 = x+glyph->bitmap_left * sx;
				float y2 = -y -(glyph->bitmap_top * sy);
				float w = glyph->bitmap.width * sx;
				float h = glyph->bitmap.rows * sy;

				float vertices[] = {
				//  Position   	  Color            	Texcoords
					x2,  -y2, r, g, b, 0.0f, 0.0f, // Top-left
					x2+w, -y2, r, g, b, 1.f, 0.0f, // Top-right
					x2+w, -y2-h, r, g, b, 1.f, 1.f, // Bottom-right
					x2, -y2-h, r, g, b, 0.0f, 1.f  // Bottom-left
				};


				glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_DYNAMIC_DRAW );
				glTexImage2D( GL_TEXTURE_2D, 0, GL_RED, glyph->bitmap.width, glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, glyph->bitmap.buffer);

				glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

				 x += (glyph->advance.x >> 6) * sx;
				 y += (glyph->advance.y >> 6) * sy;


			}
	}

	//reset
	glUseProgram(0);
	glBindVertexArray(0);

	//reset pack alignment
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
}

void Renderer::DrawLevel(glm::mat4 viewProj) {
#warning need to sort
    GLuint program = _resourceManager->GetProgram("level");
    glUseProgram(program);
    
    for(MapMap::iterator it = _maps.begin(); it != _maps.end(); ++it) {
        std::pair<GLuint, ElementCount> g = _resourceManager->GetVAO(it->first);
        Map* m = it->second;
        glBindVertexArray(g.first);
        
        glm::mat4 trans;

        
        Texture* t = _resourceManager->GetTexture(m->tileset->imageName);
        if(!t) {
            t = _resourceManager->GetTexture("cartman");
            printf("failed to retrieve texture\n");
        }
        
        glUniform1i(glGetUniformLocation(program, "overlay"), 0);
        glUniform1i(glGetUniformLocation(program, "tex"), t->slot);
        glUniformMatrix4fv(glGetUniformLocation(program, "viewProj"), 1, GL_FALSE, glm::value_ptr(viewProj));
        glUniformMatrix4fv(glGetUniformLocation(program, "trans"), 1, GL_FALSE, glm::value_ptr(trans));
        glDrawElements(GL_TRIANGLES, g.second, GL_UNSIGNED_INT, 0);
        
#warning this is dirty
        if(_levelOverlay) {
            glUniform1i(glGetUniformLocation(program, "overlay"), 1);
            glDrawElements(GL_LINES, g.second, GL_UNSIGNED_INT, 0);
            glUniform1i(glGetUniformLocation(program, "overlay"), 0);
        }

        if(_passabilityOverlay) {

            program = _resourceManager->GetProgram("pass");
        	glUseProgram(program);
        	g = _resourceManager->GetVAO("pass");
        	glBindVertexArray(g.first);
			glDrawElements(GL_TRIANGLES, g.second, GL_UNSIGNED_INT, 0);
			program = _resourceManager->GetProgram("level");
			glUseProgram(program);


        }

    }

    glUseProgram(0);
    glBindVertexArray(0);
}

void Renderer::DrawOverlay(glm::mat4 viewProj) {
    if(_passabilityOverlay) {
    		glm::mat4 trans;
    		GLuint program = _resourceManager->GetProgram("level");
    	    glUseProgram(program);
            program = _resourceManager->GetProgram("pass");
        	glUseProgram(program);
        	std::pair<GLuint, ElementCount> g = _resourceManager->GetVAO("pass");
        	glBindVertexArray(g.first);
            glUniformMatrix4fv(glGetUniformLocation(program, "viewProj"), 1, GL_FALSE, glm::value_ptr(viewProj));
            glUniformMatrix4fv(glGetUniformLocation(program, "trans"), 1, GL_FALSE, glm::value_ptr(trans));
			glDrawElements(GL_TRIANGLES, g.second, GL_UNSIGNED_INT, 0);
			program = _resourceManager->GetProgram("level");
			glUseProgram(program);
    }
    
    glUseProgram(0);
    glBindVertexArray(0);
}

void Renderer::DrawSprites(glm::mat4 viewProj) {
    std::pair<GLuint, ElementCount> g = _resourceManager->GetVAO("square");
    GLuint program = _resourceManager->GetProgram("animatedSprite");
    
    glUseProgram(program);
    glBindVertexArray(g.first);
    
    glUniformMatrix4fv(glGetUniformLocation(program, "viewProj"), 1, GL_FALSE, glm::value_ptr(viewProj));

    for(SpriteMap::iterator it = _spriteMap.begin(); it != _spriteMap.end(); ++it) {
        Sprite* sprite = it->second;
        
        glm::mat4 trans = sprite->_world * sprite->_rotate * sprite->_scale;
        glUniformMatrix4fv(glGetUniformLocation(program, "trans"), 1, GL_FALSE, glm::value_ptr(trans));
        
        Texture* t = _resourceManager->GetTexture(sprite->_texName);
        if(!t) {
            t = _resourceManager->GetTexture("cartman");
            printf("failed to retrieve texture\n");
        }
        
        //spriteSheet position
        glm::vec2 texShift;
        glm::mat2 texScale;
        if (sprite->_size.x == 0 || sprite->_size.y == 0) {
			texShift = glm::vec2(0.f,0.f);
			texScale = glm::mat2(1.f, 0.f,
								 0.f, 1.f);
        } else {;
        	texShift = sprite->_texCoord;
        	texScale = glm::mat2(sprite->_size.x / t->info.width, 0.f,
        						 0.f, sprite->_size.y / t->info.height);
        }

        glUniform2fv(glGetUniformLocation(program, "texShift"), 1, glm::value_ptr(texShift));
        glUniformMatrix2fv(glGetUniformLocation(program, "texScale"), 1, GL_FALSE, glm::value_ptr(texScale));


        glUniform1i(glGetUniformLocation(program, "tex"), t->slot);

        glDrawElements(GL_TRIANGLES, g.second, GL_UNSIGNED_INT, 0);
    }
    
    glUseProgram(0);
    glBindVertexArray(0);
}



void Renderer::Draw() {
    if(!_camera) {
        FILE_LOG(logERROR) << "(Renderer::Draw) Attempting to draw without an active camera set.";
        return;
    }
    
    glClear(GL_COLOR_BUFFER_BIT);
    glm::mat4 viewProj = _camera->GetProj() * _camera->GetView();

    DrawLevel(viewProj);
    DrawOverlay(viewProj);
    DrawSprites(viewProj);
    //DrawGUI();
    DrawText();
    
    _window->SwapBuffers();
}

void Renderer::EnableWireframe() {
    FILE_LOG(logINFO) << "(Renderer::EnableWireframe) Wireframe Mode Enabled";
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

void Renderer::DisableWireframe() {
    FILE_LOG(logINFO) << "(Renderer::DisableWireframe) Wireframe Mode Disabled";
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void Renderer::GetClientDimenstions(int* outWidth, int* outHeight) {
    _window->GetDimensions(outWidth, outHeight);
}

void Renderer::EnableLevelOverlay() {
    _levelOverlay = true;
}

void Renderer::DisableLevelOverlay() {
    _levelOverlay = false;
}

void Renderer::EnablePassabilityOverlay() {
	_passabilityOverlay = true;
}

void Renderer::DisablePassabilityOverlay() {
	_passabilityOverlay = false;
}
