//
//  Camera.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGCamera.h"
using namespace dirtygraphics;

Camera::Camera(std::string identifier, int clientWidth, int clientHeight, float fovInDegrees, float nearClip, float farClip) {
    _fovInDegrees   = fovInDegrees;
    _nearClip       = nearClip;
    _farClip        = farClip;
    _position       = glm::vec3(0.f, 0.f, 1.0f);
    _lookAt         = glm::vec3(0.f, 0.f, 0.f);
    _up             = glm::vec3(0.f, 1.f ,0.f);
    _rightt         = glm::vec3(1.f, 0.f ,0.f);
    _view           = glm::lookAt(_position, _lookAt, _up);
    _maxZoomIn      = 10.f;
    _maxZoomOut     = 10.f;
    _identifier     = identifier;
    _attached       = false;
    Resize(clientWidth, clientHeight);
}


void Camera::Move(float x, float y) {
    //flip directions because moving camera one direction should move objects other direction
    _view = glm::translate(_view, -x, -y, 0.f);
}

void Camera::MoveTo(float x, float y) {
	_view = glm::lookAt(_position, _lookAt, _up);
	_view = glm::translate(_view, -x, -y, 0.f);
}

void Camera::Resize(int clientWidth, int clientHeight) {
    _clientWidth  = clientWidth;
    _clientHeight = clientHeight;
    _aspect = (float)_clientWidth/(float)_clientHeight;
    
    _perspective = glm::perspective(_fovInDegrees, _aspect, _nearClip, _farClip);
    
    _right = (float)clientWidth/2.f;
    _left = (float)-clientWidth/2.f;
    _bottom = (float)-clientHeight/2.f;
    _top = (float)clientHeight/2.f;
    
    _orthographic = glm::ortho(_left, _right, _bottom, _top, _nearClip, _farClip);
}

void Camera::Zoom(float z) {
    _right *= z;
    _left *= z;
    _top *= z;
    _bottom *= z;
    
    // check we dont zoom in to far
    if(_right < _clientWidth/2.f/_maxZoomIn) {
        _right  =  _clientWidth  / 2.f / _maxZoomIn;
        _left   = -_clientWidth  / 2.f / _maxZoomIn;
        _top    =  _clientHeight / 2.f / _maxZoomIn;
        _bottom = -_clientHeight / 2.f / _maxZoomIn;
    }
    
    //check we dont zoom out too far
    if(_right > _clientWidth / 2.f *_maxZoomOut) {
        _right  =  _clientWidth  / 2.f * _maxZoomOut;
        _left   = -_clientWidth  / 2.f * _maxZoomOut;
        _top    =  _clientHeight / 2.f * _maxZoomOut;
        _bottom = -_clientHeight / 2.f * _maxZoomOut;
    }
    
    _orthographic = glm::ortho(_left, _right, _bottom, _top, _nearClip, _farClip);
    
}

glm::mat4 Camera::GetView() {
    return _view;
}

glm::mat4 Camera::GetProj() {
    return _orthographic;
}

void Camera::SetAttached(bool attached) {
	_attached = attached;
}
