//
//  Sprite.h
//  DirtyGraphics
//
//  Created by Eugene Sturm on 2/8/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __DirtyGraphics__Sprite__
#define __DirtyGraphics__Sprite__

#include <glm/glm.hpp>
#include <string>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include "DGRenderer.h"
#include "../ext/log.h"
#include "../animation/DASpriteAnimation.h"
#include "DGCamera.h"
using namespace dirtyAnimation;

namespace dirtygraphics {
    //forward declaration
    class Renderer;
    
    class Sprite {
    protected:
        friend class Renderer;
    
    public:
        Sprite(std::string identifier);
        
        /**
         * Set the dimensions of the displayed sprite
         * If active camera was setup properly, x and y would correspond to pixels
         * @param x size in x direction
         * @param y size in y direction
         */
        void Dimensions(float x, float y);
        
        /**
         * Move the sprite from its current position
         * @param x units in x direction to move
         * @param y units in y direction to move
         */
        void Move(float x, float y);
        
        /**
         * Assigns an animation to the sprite
         * @param animation the animation to be used
         */
        void SetAnimation(SpriteAnimation* animation);

        
        /**
         * Rotate the sprite about some axis
         * @param degrees degrees to rotate the sprite by
         * @param axis axis to rotate the sprite about (should only ever be glm::vec3(0.f, 0.f, 1.f)
         */
        void Rotate(float degrees, glm::vec3 axis);
        
        /**
         * Assign a texture to the sprite
         * Texture should already exist in the resource manager
         * @param name identifer of loaded texture
         */
        void SetTexture(std::string name);
        
        /**
         * @param x
         * @param y
         */
        void SetSize(int x, int y);
        
        /**
         * @param x
         * @param y
         */
        void Animate(float timeDif);
        
        /**
         * Move the sprite to a specific location
         * @param x x-position
         * @param y y-position
         */
        void MoveTo(float x, float y);
        
        void AttachCamera(Camera* camera);
        void DetachCamera();

    protected:
        std::string _identifier;
        glm::mat4 _scale;
        glm::mat4 _world;
        glm::mat4 _rotate;
        std::string _texName;
        glm::vec2 _texCoord;
        glm::vec2 _size;
        glm::vec2 _coords;
        int _sortKey;
        SpriteAnimation* _animation;
        Camera* _camera;
    };
}


#endif /* defined(__DirtyGraphics__Sprite__) */
