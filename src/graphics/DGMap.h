//
//  Map.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/14/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__Map__
#define __dirtygraphics__Map__

#include "DGTileset.h"
#include "DGDrawableTile.h"
#include <vector>
#include "../ext/log.h"

namespace dirtygraphics {
    class Map {
    public:
        Map();
    
        std::string name;
        int priorityLevel;
        int rows;
        int cols;
        Tileset* tileset;
        std::vector< std::vector<DrawableTile> > tiles;
    
        DrawableTile* GetTile(int r, int c);
        
        float GetMapWidth();
        float GetMapHeight();
    
    };
}

#endif /* defined(__dirtygraphics__Map__) */
