//
//  ImageLoader.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__ImageLoader__
#define __dirtygraphics__ImageLoader__

#include "DGImage.h"
#include "../ext/stb_image.h"
#include "../ext/log.h"
#include <string>

namespace dirtygraphics {
    
    class ImageLoader {
    public:
        ImageLoader();
        
        /**
         * Extracts an image file's data (png?, jpeg, gif?), allocates an Image for storage.
         * ImageLoader is not responsible for the memory it allocates
         *
         */
        Image* LoadImage(std::string fpath);

        /**
         *  Extracts a png file's data, allocates an Image for storage.
         *  ImageLoader is not responsible for the memory it allocates
         **/
        Image* LoadPNG(std::string fpath);
        
        /**
         *  Extracts a jpeg file's data, allocates an Image for storage.
         *  ImageLoader is not responsible for the memory it allocates
         **/
        Image* LoadJPEG(std::string fpath);
    };
}

#endif /* defined(__dirtygraphics__ImageLoader__) */
