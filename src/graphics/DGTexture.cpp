//
//  Texture.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGTexture.h"
using namespace dirtygraphics;

Texture::Texture(Image* image) {
    info = image->info;
    textureId = -1;
    slot = -1;
}

Texture::Texture() {
    textureId = -1;
    slot = -1;
}