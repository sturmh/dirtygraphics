//
//  Map.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/14/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGMap.h"
#include <time.h>
#include <stdlib.h>
#include <fstream>
#include <list>
using namespace dirtygraphics;

Map::Map() {
}

DrawableTile* Map::GetTile(int r, int c) {
    if(r >= rows || r < 0) return 0;
    if(c >= cols || c < 0) return 0;
    return &tiles[r][c];
}

float Map::GetMapWidth() {
    return cols * tileset->renderedTileWidth;
}


float Map::GetMapHeight() {
    return rows * tileset->renderedTileHeight;
}
