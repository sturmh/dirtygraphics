//
//  Renderer.h
//  DirtyGraphics
//
//  Created by Eugene Sturm on 2/8/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __DirtyGraphics__Renderer__
#define __DirtyGraphics__Renderer__

#ifdef __APPLE__
  #include <gl/glew.h>
#else
  #include <GL/glew.h>
#endif

#include <map>
#include <string>
#include "DGSprite.h"
#include "DGText.h"
#include <fstream>
#include <vector>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "DGCamera.h"
#include "DGResourceManager.h"
#include "../application/GLFWWindow.h"
#include "DGMap.h"
#include "DGSceneManager.h"
#include "../ext/log.h"
#include <algorithm>

#include <ft2build.h>
#include FT_FREETYPE_H

using namespace application;

namespace dirtygraphics {
    
    class Sprite;
    class SceneManager;
    
    typedef std::map<std::string, Sprite*> SpriteMap;
    typedef std::map<std::string, Map*> MapMap;
    typedef std::vector<Map*> MapVec;
    typedef std::map<std::string, Texture*> TextureMap;
    typedef std::map<std::string, Text*> TextMap;

    class Renderer {
    public:
        friend class SceneManager;
    public:
        Renderer(GLFWWindow* window, std::string workingDirectory);
        ~Renderer();

        SceneManager* GetSceneManager();
        ResourceManager* GetResourceManager();
        void EnableWireframe();
        void DisableWireframe();
        
        //TODO Make this protected once we have a gui manager
        void Submit(std::string name, Text* text);

        void EnableLevelOverlay();
        void DisableLevelOverlay();
        
        void EnablePassabilityOverlay();
        void DisablePassabilityOverlay();



        /**
         * Draw everything
         */
        void Draw();
    protected:
        void GetClientDimenstions(int* outWidth, int* outHeight);
        void SetCamera(Camera* cam);
        void Submit(std::string name, Sprite* sprite);
        void Submit(std::string name, Map* map);
        void Submit(std::string name, Map* map, core::PassabilityMap* pmap);

        void RemoveSprite(std::string name);
        void RemoveMap(std::string name);
        void RemoveText(std::string name);
    private:
        void DrawSprites(glm::mat4 viewProj);
        void DrawLevel(glm::mat4 viewProj);
        void DrawOverlay(glm::mat4 viewProj);
        void DrawGUI();
        void DrawText();
    private:
        SceneManager* _sceneManager;
        SpriteMap _spriteMap;
        TextureMap _textureMap;
        TextMap _textMap;
        MapMap _maps;
        MapMap _pmaps;
        MapVec _sortedMaps;
        Camera* _camera;
        
        ResourceManager* _resourceManager;
        
        GLFWWindow* _window;
        
        bool _levelOverlay;
        bool _passabilityOverlay;

        //text variables
        FT_Library _ft;
        FT_Face _face;

    };
}

#endif /* defined(__DirtyGraphics__Renderer__) */
