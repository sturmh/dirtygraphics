//
//  SceneManager.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/15/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__SceneManager__
#define __dirtygraphics__SceneManager__

#include "DGSprite.h"
#include "DGMap.h"
#include "DGRenderer.h"
#include "DGCamera.h"
#include "DGMap.h"
#include "../core/LevelMap/include/PassabilityMap.hh"
#include <map>

namespace dirtygraphics {
    class Renderer;
    class Sprite;
    
    typedef std::map<std::string, Sprite*> SpriteMap;
    typedef std::map<std::string, Camera*> CameraMap;
    typedef std::map<std::string, Map*> MapMap;
    typedef std::map<std::string, Tileset*> TilesetMap;
    
    class SceneManager {
    public:
        SceneManager(Renderer* renderer);
        
        /**
         * Creates an empty Map attached to provided identfier and returns it
         * @param identifier string to identify created map
         * @return empty map object
         */
        Map* CreateMap(std::string identifier);
        
        /**
         * Submits a previously created map to the renderer. This will create
         * the maps geometry and begin rendering it.
         * @param identifier identifier of map object to submit
         */
        void SubmitMap(std::string identifier);
        
        /**
		 * Submits a previously created map to the renderer. This will create
		 * the maps geometry and begin rendering it.
		 * @param identifier identifier of map object to submit
		 */
		void SubmitPassabilityMap(std::string identifier, core::PassabilityMap* pmap);

        /**
         * Removes the desired map from the renderer and frees its memory
         * @param identifier identifier of map object to destroy
         */
        void DestroyMap(std::string identifier);
        
        /**
         * Create a new empty tileset
         * @param identifier string to identify created tileset
         * @return empty tileset object
         */
        Tileset* CreateTileset(std::string identifier);
        
        /**
         * Create a new empty tileset
         * @param identifier string to identify created tileset
         * @return either null or a tileset identified by the identifier
         */
        Tileset* GetTileset(std::string identifier);
        
        /**
         * Creates a new sprite and pushes it to the renderer
         * @param identifier string to identify created sprite
         * @return default sprite object
         */
        Sprite* CreateSprite(std::string identifier);
        
        Sprite* GetSprite(std::string identifier);
        
        /**
         * Removes the desired sprite from the renderer and frees its memory
         * @param identifier identifier of sprite object to destroy
         */
        void DestroySprite(std::string identifier);
        
        /**
         * Creates a new camera with default parameters
         * @param identifier identifier of camera object
         */
        Camera* CreateCamera(std::string identifier);
        
        /**
         * Removes the desired camera from the scenemanager and frees its memory
         * @param identifier identifier of camera object to destroy
         */
        void DestroyCamera(std::string identifier);
        
        /**
         * Sets the desired camera as the active camera object in the renderer
         * @param identifier identifier of camera object to set as active
         */
        void SetActiveCamera(std::string identifier);

    private:
        Renderer* _renderer;
        SpriteMap _sprites;
        CameraMap _cameras;
        MapMap    _maps;
        TilesetMap _tilesets;
        Camera* _activeCamera;
        
    };
}

#endif /* defined(__dirtygraphics__SceneManager__) */
