//
//  Tileset.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/14/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__Tileset__
#define __dirtygraphics__Tileset__

#include <string>

namespace dirtygraphics {
    class TilesetInfo {
    public:
        
    };
    
    
    class Tileset {
    public:
        std::string name;
        std::string imageName;
        int tileHeight;
        int tileWidth;
        int renderedTileWidth;
        int renderedTileHeight;
        int spaceX;
        int spaceY;
        int maxCol;
        int maxRow;
    };
}

#endif /* defined(__dirtygraphics__Tileset__) */
