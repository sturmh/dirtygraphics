//
//  ImageLoader.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGImageLoader.h"
#include <iostream>
using namespace dirtygraphics;

ImageLoader::ImageLoader() {
    
}

Image* ImageLoader::LoadImage(std::string fpath) {
	FILE_LOG(logINFO) << "(ImageLoader::LoadImage) Loading image: " << fpath;
    Image* image = new Image();

    int width, height, comp;
    unsigned char *data;

    //FILE *file = fopen("C:\\Users\\marks_000\\git\\dirtygraphics\\media\\images\\cartman.png", "rb");
    FILE *file = fopen(fpath.c_str(), "rb");
        if (!file) {FILE_LOG(logERROR) << "(ImageLoader::LoadImage) File Open Failed: " << fpath.c_str();}

    data = stbi_load_from_file(file, &width, &height, &comp, 4);//4 should force RGBA?
    //std::vector<unsigned char> raster;
    //imagexx::raster_details details = pngxx::read_image(fpath, back_inserter(raster));

    image->info.width = width;
    image->info.height = height;
    image->info.type = UCHAR;
    image->info.format = RGBA;
    image->data = data;
    //(void)memcpy(image->data, &raster[0], sizeof(unsigned char) * raster.size());

    return image;
}

/*
Image* ImageLoader::LoadPNG(std::string fpath) {
    Image* image = new Image();
    
    std::vector<unsigned char> raster;
    imagexx::raster_details details = pngxx::read_image(fpath, back_inserter(raster));
    
    image->info.width = (int)details.width();
    image->info.height = (int)details.height();
    image->info.type = UCHAR;
    image->info.format = RGBA;
    image->data = new unsigned char[raster.size()];
    (void)memcpy(image->data, &raster[0], sizeof(unsigned char) * raster.size());
    
    return image;
}

Image* ImageLoader::LoadJPEG(std::string fpath) {
    Image* image = new Image();
    
    std::vector<unsigned char> raster;
    imagexx::raster_details details = jpegxx::read_image(fpath, back_inserter(raster));
    
    image->info.width = (int)details.width();
    image->info.height = (int)details.height();
    image->info.type = UCHAR;
    image->data = new unsigned char[image->info.width * image->info.height];
    (void)memcpy(&raster[0], image->data, sizeof(unsigned char) * image->info.width * image->info.height);
    
    return image;
}
*/
