//
//  GeometryFactory.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/14/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGGeometryFactory.h"
using namespace dirtygraphics;

Geometry GeometryFactory::GenerateSquare(float width, float height) {
    Geometry g;
    Vertex v;
    
    //tl
    v.pos = glm::vec2(-width/2.f, height/2.f);
    v.uv  = glm::vec2(0.f, 0.f);
    g.vertices.push_back(v);
    
    //tr
    v.pos = glm::vec2(width/2.f, height/2.f);
    v.uv  = glm::vec2(1.f, 0.f);
    g.vertices.push_back(v);
    
    //br
    v.pos = glm::vec2(width/2.f, -height/2.f);
    v.uv  = glm::vec2(1.f, 1.f);
    g.vertices.push_back(v);
    
    //bl
    v.pos = glm::vec2(-width/2.f, -height/2.f);
    v.uv  = glm::vec2(0.f, 1.f);
    g.vertices.push_back(v);
    
    g.indices.push_back(0);
    g.indices.push_back(1);
    g.indices.push_back(2);
    g.indices.push_back(2);
    g.indices.push_back(3);
    g.indices.push_back(0);
    
    return g;
}

Geometry GeometryFactory::GenerateMap(Map* map, ImageInfo* info) {
    Geometry g;
    Vertex tl, tr, bl, br;
    int tw = map->tileset->tileWidth;
    int th = map->tileset->tileHeight;
    float dx = map->tileset->renderedTileWidth;
    float dy = map->tileset->renderedTileHeight;
       
    float width = map->rows * dx;
    float height = map->cols * dy;
    float halfW = width / 2.f;
    float halfH = height / 2.f;
    float pixelX = 1.f / (float)info->width;
    float pixelY = 1.f / (float)info->height;
    float duOffset = map->tileset->spaceX * pixelX;
    float dvOffset = map->tileset->spaceY * pixelY;
    float u = 0.f;
    float v = 0.f;
    
    for(int i = 0; i < map->rows; ++i) {
       // printf("-----row %d----\n", i);
        for(int j = 0; j < map->cols; ++j) {
            DrawableTile* t = map->GetTile(i, j);
            if(t->visible == false) {
                continue;
            }
            
            float row = (float)t->row;
            float col = (float)t->col;
            
          //  printf("col: %d %d %d\n", j, t->col, t->row);
            
            u = ((col+1) * duOffset) + (pixelX * tw * col);
            v = ((row+1) * dvOffset) + (pixelY * th * row);
            tl.pos = glm::vec2(-halfW + (j*dx), halfH - (i*dy));
            tl.uv  = glm::vec2(u, v);
            
            u = ((col+1) * duOffset) + (pixelX * tw * (col+1));
            v = ((row+1) * dvOffset) + (pixelY * th * row);
            tr.pos = glm::vec2(-halfW + ((j+1)*dx), halfH - (i*dy));
            tr.uv  = glm::vec2(u, v);
            
            u = ((col+1) * duOffset) + (pixelX * tw * (col+1));
            v = ((row+1) * dvOffset) + (pixelY * th * (row+1));
            br.pos = glm::vec2(-halfW + ((j+1)*dx), halfH - ((i+1)*dy));
            br.uv  = glm::vec2(u, v);
            
            u = ((col+1) * duOffset) + (pixelX * tw * col);
            v = ((row+1) * dvOffset) + (pixelY * th * (row+1));
            bl.pos = glm::vec2(-halfW + (j*dx), halfH - ((i+1)*dy));
            bl.uv  = glm::vec2(u, v);
            
            if(u > 1.f || v > 1.f) {
                printf("shucks");
            }
            
            unsigned int indicesOffset = (unsigned int)g.vertices.size();
            
            g.vertices.push_back(tl);
            g.vertices.push_back(tr);
            g.vertices.push_back(br);
            g.vertices.push_back(bl);
            
            g.indices.push_back(indicesOffset + 0);
            g.indices.push_back(indicesOffset + 1);
            g.indices.push_back(indicesOffset + 2);
            g.indices.push_back(indicesOffset + 2);
            g.indices.push_back(indicesOffset + 3);
            g.indices.push_back(indicesOffset + 0);
        }
    }
    
    return g;
}

Geometry GeometryFactory::GeneratePasabilityMap(Map* map, ImageInfo* info, core::PassabilityMap* pmap) {
	Geometry g;
	Vertex tl, tr, bl, br;
	int tw = map->tileset->tileWidth;
	int th = map->tileset->tileHeight;
	float dx = map->tileset->renderedTileWidth;
	float dy = map->tileset->renderedTileHeight;

	float width = map->rows * dx;
	float height = map->cols * dy;
	float halfW = width / 2.f;
	float halfH = height / 2.f;
	float pixelX = 1.f / (float)info->width;
	float pixelY = 1.f / (float)info->height;
	float duOffset = map->tileset->spaceX * pixelX;
	float dvOffset = map->tileset->spaceY * pixelY;
	float u = 0.f;
	float v = 0.f;

	for(int i = 0; i < map->rows; ++i) {
	   // printf("-----row %d----\n", i);
		for(int j = 0; j < map->cols; ++j) {
			DrawableTile* t = map->GetTile(i, j);
			if(t->visible == false) {
				continue;
			}

			glm::vec2 passable;
			if (pmap->isPassable(i,j)) {passable = glm::vec2(0,1);}
			else {passable = glm::vec2(1,0);}

			float row = (float)t->row;
			float col = (float)t->col;

		  //  printf("col: %d %d %d\n", j, t->col, t->row);

			u = ((col+1) * duOffset) + (pixelX * tw * col);
			v = ((row+1) * dvOffset) + (pixelY * th * row);
			tl.pos = glm::vec2(-halfW + (j*dx), halfH - (i*dy));
			tl.uv  = passable;

			u = ((col+1) * duOffset) + (pixelX * tw * (col+1));
			v = ((row+1) * dvOffset) + (pixelY * th * row);
			tr.pos = glm::vec2(-halfW + ((j+1)*dx), halfH - (i*dy));
			tr.uv  = passable;

			u = ((col+1) * duOffset) + (pixelX * tw * (col+1));
			v = ((row+1) * dvOffset) + (pixelY * th * (row+1));
			br.pos = glm::vec2(-halfW + ((j+1)*dx), halfH - ((i+1)*dy));
			br.uv  = passable;

			u = ((col+1) * duOffset) + (pixelX * tw * col);
			v = ((row+1) * dvOffset) + (pixelY * th * (row+1));
			bl.pos = glm::vec2(-halfW + (j*dx), halfH - ((i+1)*dy));
			bl.uv  = passable;

			if(u > 1.f || v > 1.f) {
				printf("shucks");
			}

			unsigned int indicesOffset = (unsigned int)g.vertices.size();

			g.vertices.push_back(tl);
			g.vertices.push_back(tr);
			g.vertices.push_back(br);
			g.vertices.push_back(bl);

			g.indices.push_back(indicesOffset + 0);
			g.indices.push_back(indicesOffset + 1);
			g.indices.push_back(indicesOffset + 2);
			g.indices.push_back(indicesOffset + 2);
			g.indices.push_back(indicesOffset + 3);
			g.indices.push_back(indicesOffset + 0);
		}
	}

	return g;
}
