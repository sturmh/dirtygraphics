//
//  Texture.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__Texture__
#define __dirtygraphics__Texture__

#ifdef __APPLE__
  #include <gl/glew.h>
#else
  #include <GL/glew.h>
#endif

#include "DGImage.h"

namespace dirtygraphics {    
    class Texture {
    public:
        Texture();
        Texture(Image* image);
        
        ImageInfo info;
        GLuint textureId;
        int slot;
    };
}

#endif /* defined(__dirtygraphics__Texture__) */
