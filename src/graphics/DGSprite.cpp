//
//  Sprite.cpp
//  DirtyGraphics
//
//  Created by Eugene Sturm on 2/8/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGSprite.h"
using namespace dirtygraphics;

Sprite::Sprite(std::string identifier) {
    _identifier = identifier;
    _camera = 0;
    _coords = glm::vec2(0,0);
}

void Sprite::Dimensions(float x, float y) {
    _scale = glm::scale(_scale, x, y, 1.f);
}

void Sprite::Move(float x, float y) {
    _world = _world * glm::translate(x, y, 0.f);
    _coords = glm::vec2(_coords.x+x,_coords.y+y);
    if(_camera) {
    	_camera->Move(x,y);
    }

}

void Sprite::Rotate(float degrees, glm::vec3 axis) {
    _rotate = glm::rotate(_rotate, degrees, axis);

}

void Sprite::SetAnimation(SpriteAnimation* animation) {
    if (_animation != animation)
    {
        _animation = animation;
        if (_animation != 0) {
            _animation->ResetTime();
        }
    }
}

void Sprite::SetTexture(std::string name) {
    _texName = name;
    _texCoord = glm::vec2(0.f,0.f);
    _size = glm::vec2(0.f,0.f); //default
    _sortKey = 0; //default
}

void Sprite::SetSize(int x, int y) {
	_size = glm::vec2((float) x, (float) y);
}

void Sprite::Animate(float timeDiff) {
    if (_animation == 0) {
        //_texCoord = glm::vec2(0,0);
    } else {
        _texCoord = _animation->Animate(timeDiff); //glm uses column major order
    }
}

void Sprite::MoveTo(float x, float y) {
    _world = glm::translate(glm::mat4(), x, y, 0.f);
    _coords = glm::vec2(x,y);
    if (_camera) {
    	_camera->MoveTo(x,y);
    }
}

void Sprite::AttachCamera(Camera *camera) {
	_camera = camera;
	_camera->SetAttached(true);
	_camera->MoveTo(_coords.x,_coords.y);

}

void Sprite::DetachCamera() {
	if (_camera) {
		_camera->SetAttached(0);
		_camera = 0;
	}
}
