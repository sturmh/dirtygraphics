//
//  Camera.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__Camera__
#define __dirtygraphics__Camera__

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtx/transform.hpp>
#include <string>

namespace dirtygraphics {
    class Camera {
    public:
        
        /**
         * Creates a camera the has both a perspective and orthographic matrix
         * The orthographic matrix is sized by the orthoWidth/orthoHeight params
         * The persepective matrix is configurable, but is prob shouldnt be used
         * Camera position is defaulted to (0, 0, 1);
         * @param identifier string to identifer camera
         * @param clientWidth width of client window (used for perspective projection)
         * @param clientHeight height of client window (used for perspective projection)
         * @param fovInDegrees Field of view for perspective projection (defaulted to 45 degrees)
         * @param nearClip Position of near clipping plane in z space (defaulted to 0.5)
         * @param farClip Position of far clipping plane in z space (defaulted to 100)
         * @param orthoWidth Width of orthographic projection
         * @param orthoHeight Height of orthographic projection
         */
        Camera(std::string identifier, int clientWidth, int clientHeight, float fovInDegrees = 45.f, float nearClip = 0.5f, float farClip = 100.f);
        
        /**
         * Resizes the projection matrix to reflect a new client width and height
         * @param clientWidth width to resize projection to
         * @param clientHeight height to resize projection to
         */
        void Resize(int clientWidth, int clientHeight);
        
        /**
         * Translates the camera from its current position
         * @param x distance in x direction to translate camera
         * @param y distance in y direction to translate camera
         */
        void Move(float x, float y);
        
        /**
         * Move the camera to a specific location
         * @param x x-position
         * @param y y-position
         */
        void MoveTo(float x, float y);
        
        /**
         * Zoom resizes the orthographic projection by a factor of z
         * ex. Orthographic view set to 1600x900 with no zoom; 1 unit == 1 pixel (on a 1600x900 client)
         * Zoom in 2 -> view is set to 800x450 etc
         * @param z factor to to zoom in by
         **/
        void Zoom(float z);
        
        /**
         * Use the perspective projection as the current projection
         */
        void UsePerspective();
        
        /**
         * Use the orthographic projection as the current projection
         */
        void UseOrthographic();
        
        /**
         *  @return returns view matrix represeting position of camera
         */
        glm::mat4 GetView();
        
        /**
         * @return Returns the active projection matrix (currently orthographic) 
         */
        glm::mat4 GetProj();

        /**
         * Sets if the camera is attached to a sprite or not
         */
        void SetAttached(bool attached);
    private:
        float _fovInDegrees;
        float _aspect;
    
        float _right;
        float _left;
        float _top;
        float _bottom;
        
        float _nearClip;
        float _farClip;
        int _clientWidth;
        int _clientHeight;
        
        float _orthoWidth;
        float _orthoHeight;
        float _zoomedOrthoHeight;
        float _zoomedOrthoWidth;
        
        float _currentZoom;
        float _maxZoomIn;
        float _maxZoomOut;
        
        glm::vec3 _position;
        glm::vec3 _lookAt;
        glm::vec3 _up;
        glm::vec3 _rightt;
        
        glm::mat4 _view;
        glm::mat4 _perspective;
        glm::mat4 _orthographic;
        
        bool _useOrtho;
        
        std::string _identifier;

        bool _attached;
    };
}

#endif /* defined(__dirtygraphics__Camera__) */
