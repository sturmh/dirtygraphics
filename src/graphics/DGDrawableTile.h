//
//  DrawableTile.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/14/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__DrawableTile__
#define __dirtygraphics__DrawableTile__


namespace dirtygraphics {
    class DrawableTile {
    public:
        DrawableTile();
        int row;
        int col;
        bool visible;
    };
}

#endif /* defined(__dirtygraphics__DrawableTile__) */
