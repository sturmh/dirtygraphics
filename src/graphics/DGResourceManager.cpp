//
//  ResourceManager.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGResourceManager.h"
using namespace dirtygraphics;

ResourceManager::ResourceManager(std::string workingDirectory) {
    _workingDirectory = workingDirectory;
    _textureSlotIndex = 0;
        
  /*  if(FT_Init_FreeType(&_ft)) {
        fprintf(stderr, "Could not init freetype library\n");
        return;
    }*/
    
  //  LoadFont("FreeSans", 48, "/media/fonts/FreeSans.ttf");
    
    if(GLSL_VERSION == 150) {
        _shaderPath = "/assets/shaders/shaders150/";
    } else if(GLSL_VERSION == 120) {
        _shaderPath = "/assets/shaders/shaders120/";
    }
    
    LoadProgram("animatedSprite", "animatedSprite.frag", "animatedSprite.vert");
    LoadProgram("level", "level.frag", "level.vert");
    LoadProgram("text", "text.frag", "text.vert");
    LoadProgram("pass", "pass.frag", "pass.vert");
//    LoadProgram("gui", "/assets/shaders/gui.frag", "/assets/shaders/gui.vert");
    
    
    CreateGeometry("square", _geometryFactory.GenerateSquare());
}

ResourceManager::~ResourceManager() {
    
}
/*
void ResourceManager::LoadFont(std::string name, int fontsize, std::string fpath) {
    FT_Face face;
    
    if(FT_New_Face(_ft, (_workingDirectory + fpath).c_str(), 0, &face)) {
        fprintf(stderr, "Could not open font\n");
        return;
    }
    
    FT_Set_Pixel_Sizes(face, 0, fontsize);
    
    _fonts[name] = face;
}

FT_Face ResourceManager::GetFont(std::string name) {
    return _fonts[name];
}
*/
void ResourceManager::CreateGeometry(std::string name, Geometry g) {
	FILE_LOG(logINFO) << "(ResourceManager::CreateGeometry) Creating Geometry: " << name;

    GLuint vao;
    GLuint vbo;
    GLuint ebo;
    
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
    
    // Create a Vertex Buffer Object and copy the vertex data to it
	glGenBuffers(1, &vbo );
	glBindBuffer(GL_ARRAY_BUFFER, vbo );
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*g.vertices.size(), &g.vertices[0], GL_STATIC_DRAW);
    
    //Create element buffer

    glGenBuffers( 1, &ebo );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, ebo );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(unsigned int)*g.indices.size(), &g.indices[0], GL_STATIC_DRAW );
    
    
	// Specify the layout of the vertex data
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0 );
    
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)( 2*sizeof(float)));
    
    
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    _activeVAOs[name] = std::make_pair(vao, g.indices.size());

    GLenum error = glGetError();
    while (error != GL_NO_ERROR)
    {
    	FILE_LOG(logERROR) << "(ResourceManager::CreateGeometry) OpenGL Error: " << error;
    	error = glGetError();
    }
}

void ResourceManager::LoadTexture(std::string name, std::string fpath) {
	FILE_LOG(logINFO) << "(ResourceManager::LoadTexture) Loading Texture: " << name;
	Image* image = _imageLoader.LoadImage(_workingDirectory + fpath);
    BindTexture(name, image);
    delete image;
}

Texture* ResourceManager::GetTexture(std::string name) {
    return _activeTextures[name];
}

int ResourceManager::GetTextureSlot() {
    return (_textureSlotIndex++)%32;
}

GLuint ResourceManager::GetProgram(std::string name) {
    return _programs[name];
}

std::pair<GLuint, ElementCount> ResourceManager::GetVAO(std::string name) {
    return _activeVAOs[name];
}

Texture* ResourceManager::BindTexture(std::string name, Image* image) {
    Texture* texture = new Texture();
    
    texture->slot = GetTextureSlot();
    
    glGenTextures(1, &texture->textureId);
    
    //temporary
    glActiveTexture(GL_TEXTURE0 + texture->slot);
    
    
    glBindTexture(GL_TEXTURE_2D, texture->textureId);
    
    //do i have to do this everytime?
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );

    if(image) {
        //map pixelformat to glenum
        GLenum format;
        if(image->info.format == RGB) {
            format = GL_RGB;
        } else if(image->info.format == RGBA) {
            format = GL_RGBA;
        } else {
            format = GL_RGBA;
        }
        
        //map pixeltype to glenum
        GLenum type;
        if(image->info.type == FLOAT) {
            type = GL_FLOAT;
        } else if(image->info.type == UCHAR) {
            type = GL_UNSIGNED_BYTE;
        } else {
            type = GL_FLOAT;
        }
        
        //store image data in texture
        glTexImage2D(GL_TEXTURE_2D, 0, format, image->info.width, image->info.height, 0, format, type, image->data);
        
        
        texture->info = image->info;
    }
   
    
    _activeTextures[name] = texture;

    GLenum error = glGetError();
    while (error != GL_NO_ERROR)
    {
    	FILE_LOG(logERROR) << "(ResourceManager::BindTexture) OpenGL Error: " << error;
    	error = glGetError();
    }

    return texture;
}

void ResourceManager::LoadProgram(std::string name, std::string fragPath, std::string vertPath) {
    GLint status;
    
    FILE_LOG(logINFO) << "(ResourceManager::LoadProgram) Loading Program: " << name;

    std::string vertexSource = LoadFile(vertPath);
    if(vertexSource == "") {
    	FILE_LOG(logERROR) << "(ResourceManager::LoadProgram) vertex shader load failed: " << vertPath;
        return;
    }
    
    std::string fragmentSource = LoadFile(fragPath);
    if(fragmentSource == "") {
    	FILE_LOG(logERROR) << "(ResourceManager::LoadProgram) fragment shader load failed: " << fragPath;
        return;
    }
    
    // Create and compile the vertex shader
	GLuint vertexShader = glCreateShader( GL_VERTEX_SHADER );
    const char* vc = vertexSource.c_str();
	glShaderSource( vertexShader, 1, &vc, NULL );
	glCompileShader( vertexShader );
    
    //check for errors
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    if(status != GL_TRUE) {
        char buffer[512];
        glGetShaderInfoLog(vertexShader, 512, NULL, buffer);
        printf("Failed to compile shader");
        FILE_LOG(logERROR) << "(ResourceManager::LoadProgram) vertex shader compile failed";
        FILE_LOG(logERROR) << "(ResourceManager::LoadProgram) Reported shader error " << buffer;
        return;
    }
    
	// Create and compile the fragment shader
	GLuint fragmentShader = glCreateShader( GL_FRAGMENT_SHADER );
    const char* fc = fragmentSource.c_str();
	glShaderSource( fragmentShader, 1, &fc, NULL );
	glCompileShader( fragmentShader );
    
    //check for errors
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
    if(status != GL_TRUE) {
        char buffer[512];
        glGetShaderInfoLog(fragmentShader, 512, NULL, buffer);
        printf("Failed to compile shader");
        FILE_LOG(logERROR) << "(ResourceManager::LoadProgram) fragment shader compile failed";
                FILE_LOG(logERROR) << "(ResourceManager::LoadProgram) Reported shader error " << buffer;
        return;
    }
    
	// Link the vertex and fragment shader into a shader program
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader );
	glAttachShader(shaderProgram, fragmentShader );
	glBindFragDataLocation(shaderProgram, 0, "outColor" );
    glBindAttribLocation(shaderProgram, 0, "pos");
    glBindAttribLocation(shaderProgram, 1, "uv");
    
	glLinkProgram(shaderProgram);
    
    //check for linking problems
    glGetProgramiv (shaderProgram, GL_LINK_STATUS, &status);
    if (status == GL_FALSE)
    {
        GLint infoLogLength;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
        
        GLchar *strInfoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(shaderProgram, infoLogLength, NULL, strInfoLog);
        delete[] strInfoLog;
    }

    _programs[name] = shaderProgram;

    GLenum error = glGetError();
    while (error != GL_NO_ERROR)
    {
    	FILE_LOG(logERROR) << "(ResourceManager::LoadProgram) OpenGL Error: " << error;
    	error = glGetError();
    }
}

std::string ResourceManager::LoadFile(std::string fpath) {
    std::string content = "";
    std::ifstream fin;
    
    fin.open((_workingDirectory + _shaderPath + fpath).c_str());
    if(fin.fail()) {
    	FILE_LOG(logERROR) << "(ResourceManager::LoadFile) File Load Failed: " << (_workingDirectory + _shaderPath + fpath).c_str();
        return content;
    }
    fin.seekg(0, std::ios::end);
    content.resize(fin.tellg());
    fin.seekg(0, std::ios::beg);
    fin.read(&content[0], content.size());
    fin.close();
    
    return content;
}
