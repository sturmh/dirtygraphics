//
//  SceneManager.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/15/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGSceneManager.h"
using namespace dirtygraphics;

SceneManager::SceneManager(Renderer* renderer) {
    _renderer = renderer;
    _activeCamera = 0;
}

Map* SceneManager::CreateMap(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::CreateMap) Creating a new map (" << identifier << ")";

    Map* map = new Map();
    if(_maps[identifier]) {
        FILE_LOG(logERROR) << "(SceneManager::CreateMap) Overwriting previously created map (" << identifier << ")";
    }
    
    map->name = identifier;
    _maps[identifier] = map;
    return map;
}



void SceneManager::SubmitMap(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::SubmitMap) Submitting Map (" << identifier << ")";
    Map* map = _maps[identifier];
    if(map != 0) {
        //check everythingis filled in
        _renderer->Submit(identifier, map);
    } else {
        FILE_LOG(logERROR) << "(SceneManager::SubmitMap) Submitting Map that doesnt exist (" << identifier << ")";
    }
    
}

void SceneManager::SubmitPassabilityMap(std::string identifier, core::PassabilityMap* pmap) {
    FILE_LOG(logINFO) << "(SceneManager::SubmitMap) Submitting PassabilityMap (" << identifier << ")";
    Map* map = _maps[identifier];
    if(map != 0) {
        //check everythingis filled in
        _renderer->Submit(identifier, map, pmap);
    } else {
        FILE_LOG(logERROR) << "(SceneManager::SubmitMap) Submitting Map that doesnt exist (" << identifier << ")";
    }

}

void SceneManager::DestroyMap(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::DestroyMap) Destroying Map (" << identifier << ")";
    Map* map = _maps[identifier];
    if(map != 0) {
        _renderer->RemoveMap(identifier);
        delete map;
        map = 0;
        _maps.erase(identifier);
    } else {
         FILE_LOG(logERROR) << "(SceneManager::DestroyMap) Destroying map that doesnt exist (" << identifier << ")";
    }
}

Tileset* SceneManager::CreateTileset(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::CreateTileset) Creating tileset (" << identifier << ")";
    Tileset* t = new Tileset();
    t->name = identifier;
    _tilesets[identifier] = t;
    return t;
}

Tileset* SceneManager::GetTileset(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::GetTileset) Get tileset(" << identifier << ")";
    return _tilesets[identifier];
}

Sprite* SceneManager::CreateSprite(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::CreateSprite) Creating sprite (" << identifier << ")";
    Sprite* sprite = new Sprite(identifier);
    
    if(_sprites[identifier]) {
        FILE_LOG(logERROR) << "(SceneManager::CreateSprite) Overwriting already created sprite (" << identifier << ")";
    }
    
    _sprites[identifier] = sprite;
    _renderer->Submit(identifier, sprite);
    
    return sprite;
}

Sprite* SceneManager::GetSprite(std::string identifier) {
    Sprite* sprite = _sprites[identifier];
    if(!sprite) {
        FILE_LOG(logINFO) << "(SceneManager::GetSprite) Attempting to retrieve sprite that doesnt exist (" << identifier << ")";
    }
    return sprite;
}

void SceneManager::DestroySprite(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::DestroySprite) Destroying sprite (" << identifier << ")";
    Sprite* sprite = _sprites[identifier];
    if(sprite) {
        _renderer->RemoveSprite(identifier);
        delete sprite;
        sprite = 0;
        _sprites.erase(identifier);
    } else {
      FILE_LOG(logERROR) << "(SceneManager::DestroySprite) Destroying sprite that doesnt exist (" << identifier << ")";
    }
    
}

Camera* SceneManager::CreateCamera(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::CreateCamera) Creating camera (" << identifier << ")";
    int width, height;
    
    _renderer->GetClientDimenstions(&width, &height);
    Camera* camera = new Camera(identifier, width, height);

    if(_cameras[identifier]) {
         FILE_LOG(logERROR) << "(SceneManager::CreateCamera) Overwriting already created camera (" << identifier << ")";
    }
    
    _cameras[identifier] = camera;
    
    return camera;
}

void SceneManager::DestroyCamera(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::DestroyCamera) destroying camera (" << identifier << ")";
    Camera* camera = _cameras[identifier];
    if(camera) {
        if(camera == _activeCamera) {
            FILE_LOG(logERROR) << "(SceneManager::DestroyCamera) Attempting to destroy active camera";
        } else {
            delete camera;
            camera = 0;
            _cameras.erase(identifier);
        }
    } else {
       FILE_LOG(logERROR) << "(SceneManager::DestroyCamera) Attempting to destroy camera that doesnt exists"; 
    }
}

void SceneManager::SetActiveCamera(std::string identifier) {
    FILE_LOG(logINFO) << "(SceneManager::SetActiveCamera) Setting active camera (" << identifier << ")";
    Camera* camera = _cameras[identifier];
    if(camera) {
        _activeCamera = camera;
        _renderer->SetCamera(camera);
    } else {
    FILE_LOG(logERROR) << "(SceneManager::SetActiveCamera) Trying to set active camera to camera that doesnt exists (" << identifier << ")";
    }
}
