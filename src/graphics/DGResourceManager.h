//
//  ResourceManager.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__ResourceManager__
#define __dirtygraphics__ResourceManager__

#include "DGImageLoader.h"
#include "DGTexture.h"
#include <map>
#include <utility>

#ifdef __APPLE__
  #include <gl/glew.h>
#else
  #include <GL/glew.h>
#endif

#include "DGGeometryFactory.h"
#include "DGMap.h"
#include <fstream>
#include "Config.h"
//#include <ft2build.h>
//#include FT_FREETYPE_H

namespace dirtygraphics {
    typedef unsigned int ElementCount;
    
    class ResourceManager {
    public:
        friend class Renderer;
    public:
        ResourceManager(std::string workingDirectory);
        ~ResourceManager();
        
        /**
         * Loads an image into the renderer
         * @param name string to identify loaded image
         * @param fpath path to image relative to working directory
         */
        void LoadTexture(std::string name, std::string fpath);
        
        void LoadTileset(std::string name, TilesetInfo info, std::string fpath);
        
    protected:
        void CreateGeometry(std::string name, Geometry g);
        void LoadProgram(std::string name, std::string fragPath, std::string vertPath);
     //   void LoadFont(std::string name, int fontsize, std::string fpath);
     //   FT_Face GetFont(std::string name);
        GLuint GetProgram(std::string name);
        Texture* GetTexture(std::string name);
        std::pair<GLuint, ElementCount> GetVAO(std::string name);
        
        /**
         *  Creates a GL texture and binds the stores the image data in it
         */
        Texture* BindTexture(std::string name, Image* image);
    private:
        std::string LoadFile(std::string fpath);
        int GetTextureSlot();
    private:
        std::string _workingDirectory;
        std::string _shaderPath;
        ImageLoader _imageLoader;
        std::map<std::string, Texture*> _activeTextures;
        std::map<std::string, std::pair<GLuint, ElementCount> > _activeVAOs;
        std::map<std::string, GLuint> _programs;
        //std::map<std::string, FT_Face> _fonts;
        int _textureSlotIndex;
        GeometryFactory _geometryFactory;
        //FT_Library _ft;
        
    };
}

#endif /* defined(__dirtygraphics__ResourceManager__) */
