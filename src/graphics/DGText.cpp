/*
 * Text.cpp
 *
 *  Created on: Feb 23, 2013
 *      Author: marks_000
 */

#include "DGText.h"

namespace dirtygraphics {
	Text::Text() {
		//default to black in the center
		_rgb = glm::vec3(0.f,0.f,0.f);
		_pos = glm::vec2(0.f,0.f);
		_text = "!__TEXT_NOT_SET__!";
		_text = "!!!!!!TEXT NOT SET!!!!!";
	}

	void Text::SetText(std::string text) {
		_text = text;
	}

	void Text::SetColor(float r, float g, float b) {
		_rgb = glm::vec3(r,g,b);
	}

	/*
	 * Set the position of the text in OpenGL coordinates (-1,1)
	 */
	void Text::SetPosition(float x, float y) {
		_pos = glm::vec2(x,y);
	}

	std::string Text::GetText() {
		return _text;
	}

	glm::vec3 Text::GetColor() {
		return _rgb;
	}

	glm::vec2 Text::GetPosition() {
		return _pos;
	}

}
