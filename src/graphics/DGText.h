/*
 * Text.h
 *
 *  Created on: Feb 23, 2013
 *      Author: marks_000
 */

#ifndef TEXT_H_
#define TEXT_H_

#include <string>
#include <glm/glm.hpp>

namespace dirtygraphics {
    class Text {
    public:
    	Text();
        void SetText(std::string text);
        void SetColor(float r, float g, float b);
        void SetPosition(float x, float y);

        std::string GetText();
        glm::vec3 GetColor();
        glm::vec2 GetPosition();

        std::string _text;
        glm::vec3 _rgb;
        glm::vec2 _pos;

    };
}
#endif /* TEXT_H_ */
