//
//  GeometryFactory.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/14/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__GeometryFactory__
#define __dirtygraphics__GeometryFactory__

#include <vector>
#include <glm/glm.hpp>
#include "DGImage.h"
#include "DGMap.h"
#include "../core/LevelMap/include/PassabilityMap.hh"

namespace dirtygraphics {
    struct Vertex {
        glm::vec2 pos;
        glm::vec2 uv;
    };
    struct Geometry {
        std::vector<Vertex> vertices;
        std::vector<unsigned int> indices;

    };
    
    class GeometryFactory {
    public:
        Geometry GenerateSquare(float width = 1.f, float height = 1.f);
        Geometry GenerateMap(Map* map, ImageInfo* info);
        Geometry GeneratePasabilityMap(Map* map, ImageInfo* info, core::PassabilityMap* pmap);
            

    private:
    };
}

#endif /* defined(__dirtygraphics__GeometryFactory__) */
