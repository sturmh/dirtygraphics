//
//  Tile.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/14/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGDrawableTile.h"
using namespace dirtygraphics;

DrawableTile::DrawableTile() {
    visible = true;
    row = 0;
    col = 0;
}