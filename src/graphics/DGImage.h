//
//  Image.h
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#ifndef __dirtygraphics__Image__
#define __dirtygraphics__Image__

namespace dirtygraphics {
    enum PixelFormat {
        RGB,
        RGBA,
        GREY,
        GREY_ALPHA,
        UNKNOWN,
    };
    
    enum PixelType {
        UCHAR,
        FLOAT,
        NONE
    };
    
    class ImageInfo {
    public:
        ImageInfo();
        
        PixelType type;
        PixelFormat format;
        int width;
        int height;
    };
    
    class Image {
    public:
        ~Image();
        
        ImageInfo info;
        unsigned char* data;
    };
}

#endif /* defined(__dirtygraphics__Image__) */
