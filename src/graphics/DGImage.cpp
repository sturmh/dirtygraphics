//
//  Image.cpp
//  dirtygraphics
//
//  Created by Eugene Sturm on 2/11/13.
//  Copyright (c) 2013 Eugene Sturm. All rights reserved.
//

#include "DGImage.h"
using namespace dirtygraphics;

ImageInfo::ImageInfo() {
    width = 0;
    height = 0;
    format = UNKNOWN;
    type = NONE;
}

Image::~Image() {
    if(data) {
        delete [] data;
    }
}