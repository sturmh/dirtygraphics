/*
 * AnimationManager.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: marks_000
 */

#include "DAAnimationManager.h"
using namespace dirtyAnimation;

AnimationManager::AnimationManager() {

}

SpriteAnimation* AnimationManager::CreateSpriteAnimation(std::string identifier, bool loop) {
    Animation* animation = new SpriteAnimation(identifier, loop);
    _animations[identifier] = animation;
    return (SpriteAnimation*)animation;
}

void AnimationManager::DestroySpriteAnimation(std::string identifier) {
    Animation* animation = _animations[identifier];
    if (animation) {
        delete animation;
        animation = 0;
        _animations.erase(identifier);
    }
}



