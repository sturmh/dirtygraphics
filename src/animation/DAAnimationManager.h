/*
 * AnimationManager.h
 *
 *  Created on: Feb 19, 2013
 *      Author: marks_000
 */

#ifndef ANIMATIONMANAGER_H_
#define ANIMATIONMANAGER_H_

#include "DASpriteAnimation.h"
#include <map>


namespace dirtyAnimation {
    class Animation;
    typedef std::map<std::string, Animation*> AnimationMap;

    class AnimationManager {
    public:
        AnimationManager ();
        SpriteAnimation* CreateSpriteAnimation(std::string identifier, bool loop);
        void DestroySpriteAnimation(std::string identifier);

    private:
        AnimationMap _animations;
    };







}

#endif /* ANIMATIONMANAGER_H_ */
