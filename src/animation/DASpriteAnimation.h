/*
 * SpriteAnimation.h
 *
 *  Created on: Feb 19, 2013
 *      Author: marks_000
 */

#ifndef SPRITEANIMATION_H_
#define SPRITEANIMATION_H_

#include "glm/glm.hpp"
#include "DAAnimation.h"
#include <vector>

namespace dirtyAnimation
{

class SpriteAnimation: public dirtyAnimation::Animation
{
public:
    SpriteAnimation(std::string identifier, bool loop);
    glm::vec2 Animate(float timeDif);
    void AddState(glm::vec2 state, float time);
    void ResetTime();
    void ClearStates();
private:
    std::vector<float> _stateTimes;
    std::vector<glm::vec2> _states;
    float _animationTime;
    float _totalTime;
    bool _loop;
};

} /* namespace dirtygraphics */
#endif /* SPRITEANIMATION_H_ */
