/*
 * SpriteAnimation.cpp
 *
 *  Created on: Feb 19, 2013
 *      Author: marks_000
 */

#include "DASpriteAnimation.h"
#include <iostream>

namespace dirtyAnimation
{

SpriteAnimation::SpriteAnimation(std::string identifier, bool loop)
{
    _identifier = identifier;
    _loop = loop;
    _animationTime = 0.f;
    _totalTime = 0.f;
}

glm::vec2 SpriteAnimation::Animate(float timeDif) {
    _animationTime+=timeDif;
    if (_loop)
    {
        if (_animationTime > _totalTime)
        {
            while (_animationTime > _totalTime) _animationTime -= _totalTime;
        }
    } else {
        if (_animationTime > _totalTime)
        {
            return glm::vec2(0,0);
        }
    }

    float time = _animationTime;
    for (int i = 0; i < (int) _stateTimes.size(); i++)
    {
        time-=_stateTimes.at(i);
        if (time <= 0)
        {
            return _states[i];
        }
    }

    return glm::vec2(0,0);
}

void SpriteAnimation::AddState(glm::vec2 state, float time)
{
    _states.push_back(state);
    _stateTimes.push_back(time);
    _totalTime += time;
}

void SpriteAnimation::ResetTime()
{
    _animationTime = 0;
}

void SpriteAnimation::ClearStates()
{
    _stateTimes.clear();
    _states.clear();
    _totalTime = 0.f;
}


} /* namespace dirtygraphics */
